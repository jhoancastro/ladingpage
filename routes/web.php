<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('lading', 'LadingController');
Route::get('/citysl', 'LadingController@getCitys');
Route::get('ladingexport', 'LadingController@exportExcel')->name('lading.export');
Route::post('lading', 'LadingController@store')->name('lading.store');


Route::get('/home', 'HomeController@index')->name('home');
