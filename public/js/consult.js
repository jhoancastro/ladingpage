$(document).ready(function() {
	$('#departamentlist').on('change', function() {
		var departament_id = $(this).val();
		if ($.trim(departament_id) != '') { 
			$.get('citysl', {id:departament_id}, function(citysl) {
				$('#citylist').empty();
				$('#citylist').append("<option value=''>Selecciona Un Departamento</option>");
				$.each(citysl, function (index, value) {
					$('#citylist').append("<option value='"+ index +"'>"+ value +"</option>");
					
				});
			});
		}
	});
});