@extends('layouts.app')
@section('content')
<div id="caja">
	<section class="container">
		<div class="row justify-content-left">

			<div id="cont-ladin" class="col-md-5 mt-3 mb-3">
				@include('lading.fragment.error')

				{!! Form::open(['route' => 'lading.store', 'method' => 'post', 'novalidate' ]) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Apellido</label>
						<input type="text" name="lastname" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Identificacion</label>
						<input type="number" name="identification" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Numero</label>
						<input type="number" name="number" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Correo</label>
						<input type="email" name="email" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Departamentos</label>
						<select class="form-control" id="departamentlist" name="departament">
							<option selected>...</option>
							@foreach($departaments as $departament)
							<option value="{{ $departament->id }}">{{ $departament->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label>Ciudades</label>
						<select class="form-control" id="citylist" name="city">
							<option selected>...</option>
						</select>
					</div>
					<div class="form-group">
						<div class="form-check" required>
	                        <input class="form-check-input" type="checkbox" id="gridCheck" required>
	                        <label class="form-check-label" for="gridCheck"> Acepto todos los términos y condiciones </label>
	                    </div>
					</div>
					<div  id="btnm" class="form-group">
						<button type="subit" class="btn btn-success">Registar</button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</section>
</div>
@endsection