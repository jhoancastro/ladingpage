<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class customers extends Model
{
    Protected $table = 'customers';
    protected $fillable = ['date', 'name','lastname','identification','departament','city','number','email'];
    protected $guarded = ['id'];
}
