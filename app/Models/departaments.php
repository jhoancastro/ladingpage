<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class departaments extends Model
{
    Protected $table = 'departaments';
    protected $fillable = ['name','customer_id'];
    protected $guarded = ['id'];
}
