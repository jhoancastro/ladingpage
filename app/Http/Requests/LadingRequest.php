<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LadingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'identification' => 'numeric|required|unique:customers|min:100000|max:9999999999',
            'departament' => 'required|max:255',
            'city' => 'required|max:255',
            'number' => 'numeric|required',
            'email' => 'required|email|max:255|unique:users'
        ];
    }

    public function messages()
    {
      return [
          'name.required' => 'El campo nombre es obligatorio',
          'lastname.required' => 'El campo apellido es obligatorio',
          'departament.required' => 'El campo departamento es obligatorio',
          'city.required' => 'El campo ciudad es obligatorio',
          'email.required' => 'El campo correo es obligatorio',
      ];
    }
}
