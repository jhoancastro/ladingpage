<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class citys extends Model
{
    Protected $table = 'citys';
    protected $fillable = ['name','departament_id'];
    protected $guarded = ['id'];
}
