<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Departaments as Departaments;
use App\Models\Citys as Citys;
use App\Models\Customers as Customers;

use App\Http\Requests\LadingRequest;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CustomersExport;

class LadingController extends Controller
{
     public function index()
	{
		$departaments = Departaments::all();
		return \View::make('lading/lading' ,compact('departaments'));
	}

	public function getCitys(Request $request)
	{
		if ($request->ajax()) {
			$citys = Citys::where('departament_id', $request->id)->get();
			foreach ($citys as $city) {
				$cityArray[$city->id] = $city->name;
			}
			return response()->json($cityArray);
		}
	}

	public function store(LadingRequest $request)
	{
		$ldate = date('Y-m-d H:i:s');
		$customers = new Customers;
	    $customers->date = $ldate;
	    $customers->name = $request->name;
	    $customers->lastname = $request->lastname;
	    $customers->identification = $request->identification;
	    $customers->departament = $request->departament;
	    $customers->city = $request->city;
	    $customers->number = $request->number;
	    $customers->email = $request->email;
	    $customers->save();
	    return redirect('lading');
	}

	public function exportExcel()
	{

        return Excel::download(new CustomersExport, 'customers-list.xlsx');
    }
}
